#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

int isfc(char* str){
	return str[0] != '/';
}

int undeb(char* pkg, char* dir){
	char y[1024],z[1024];
	strcpy(z,"ar x ");
	if(isfc(pkg) != 0){
		if(getcwd(y,sizeof(y)) == NULL){
			fprintf(stderr,"You MUST write full package location\n");
			return 1;
		}
		strcat(z,y);
		strcat(z,"/");
	}
	strcat(z,pkg);
	chdir(dir);
	if(system(z)) return 1;
	strcpy(y,dir);
	strcat(y,"/control");
	mkdir(y,0755);
	strcpy(y,dir);
	strcat(y,"/data");
	mkdir(y,0755);
	strcpy(z,"tar xzf ");
	strcat(z,dir);
	strcat(z,"/control.tar.gz -C ");
	strcat(z,dir);
	strcat(z,"/control/");
	if(system(z)) return 1;
	strcpy(z,"tar xJf ");
	strcat(z,dir);
	strcat(z,"/data.tar.xz -C ");
	strcat(z,dir);
	strcat(z,"/data/");
	if(system(z)) return 1;
	strcpy(z,"rm -rf ");
	strcat(z,dir);
	strcat(z,"/control.tar.gz ");
	strcat(z,dir);
	strcat(z,"/data.tar.xz ");
	strcat(z,dir);
	strcat(z,"/debian-binary");
	if(system(z)) return 1;
	return 0;
}

int main(int argc, char* argv[]){
	char x[128],y[1024];
	if(argc < 2 || argc > 3){
		strcpy(x,"Usage: ");
		strcat(x,argv[0]);
		strcat(x," <deb-file> [DIR");
		if(getcwd(y,sizeof(y)) != NULL){
			strcat(x," (default: ");
			strcat(x,y);
			strcat(x,")");
		}
		strcat(x,"]\n");
		fprintf(stderr,x);
		return 1;
	}
	
	if(argc == 2){
		if(getcwd(y,sizeof(y)) == NULL){
			fprintf(stderr,"You MUST write a DIR argument\n");
			return 1;
		}
	} else {
		DIR* dir = opendir(argv[2]);
		if(dir) strcpy(y,argv[2]);
		else {
			if(mkdir(argv[2],0755)){
				fprintf(stderr,"DIR argument is invalid\nYou MUST create this folder manually and make it accessible\n");
				return 1;
			}
			strcpy(y,argv[2]);
		}
		closedir(dir);
	}
	
	if(undeb(argv[1],y) != 0){
		fprintf(stderr,"A child process returned with error\n");
		return 1;
	}
	return 0;
}
